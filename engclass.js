const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
const morgan = require('morgan');
const cors = require('cors');
const { format } = require('morgan');
const csvtojson = require('csvtojson');
const fs = require('fs');
const mysql = require('mysql');
let app = express();
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('html'));
app.use(express.static(__dirname));
const { forEachOf } = require('async');
//data base
const con = mysql.createConnection({
    host: 'localhost',
    user: "root",
    password: "",
    database: "engdb"
})
con.connect((err) => {
    if (err) {
        return console.error('error: ' + err.message);
    }
    console.log('Connection connecting to DB Succeed');
})
//ประกาศตัวแปร
var teaid = "";
var tea_name = "";
var tea_lastname = "";
var id_class = '';
var id_lesson = "";
var id_seclesson = "";
var id_sectest = "";
var stuID = "";
var stuNum = "";
var id_testSTU = "";
var id_stu = "";
var id_view_score = "";

//อัพโหลดไฟล์
const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        if (file.fieldname === "fileImg") {
            cb(null, './image/');
        } else if (file.fieldname === "doc") { // if uploading pdf
            cb(null, 'doc');
        } else if (file.fieldname === "vdo") { // else uploading video
            cb(null, 'vdo');
        } else if (file.fieldname === "uploadfileScore") { // else uploading video
            cb(null, './uploads/');
        } else if (file.fieldname === "fileStu") { // else uploading video
            cb(null, './uploads/');
        } else {
            cb(null, 'docTest');
        }
    },
    filename: (req, file, cb) => { // naming file
        cb(null, file.originalname);
    }
});
var uploadfile = multer({ storage: fileStorage })

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/html/" + "login-page.html");
});
app.get("/loginpagetea", function (req, res) {
    res.sendFile(__dirname + "/html/" + "login-page-tea.html");
});
app.post("/logintea", function (req, res) {
    var username = req.body.username
    var password = req.body.password
    console.log(username);
    console.log(password);
    /* if (username == "61160144" && password == "123") {
        res.redirect('/class')
    } */
    if (username && password) {
        con.query('SELECT * FROM teacher WHERE tea_username = ? AND tea_password = ?', [username, password], (err, result) => {
            if (result.length > 0) {
                teaid = JSON.stringify(result[0].tea_id).replace(/"(.*)"$/, '$1')
                username = JSON.stringify(result[0].tea_username).replace(/"(.*)"$/, '$1')
                password = JSON.stringify(result[0].tea_password).replace(/"(.*)"$/, '$1')
                res.redirect('/class');
            } else {
                res.send('<script>alert("คุณไม่เป็นสมาชิก กรุณาลองอีกครั้ง!"); window.location.href = "/loginpagetea"; </script>');
            }
        });
    } else {
        res.send('<script>alert("กรอกข้อมูลไม่ถูกต้อง กรุณาลองอีกครั้ง!"); window.location.href = "/loginpagetea"; </script>');
    }
});
//โชว์หน้าลงทะเบียน
app.get("/registea", function (req, res) {
    res.sendFile(__dirname + "/html/" + "sigup-page.html");
});
//เพิ่มครูเข้าดาต้า
app.post('/addTea', (req, res) => {
    const name = req.body.firstname;
    const lname = req.body.lastname;
    const user = req.body.username;
    const password = req.body.password;
    if (user && password) {
        con.query('SELECT * FROM teacher WHERE tea_username = ?', [user], (err, result) => {
            if (result.length > 0) {
                if (err) throw err;
                res.send('<script>alert("กรอกข้อมูลชื่อผู้ใช้ซ้ำ กรุณาลองอีกครั้ง!"); window.location.href = "/registea"; </script>');
                //return res.render(__dirname + "/html/" + "sigup-page.html");
            } else {
                var Teacher = {
                    tea_firstname: name,
                    tea_lastname: lname,
                    tea_username: user,
                    tea_password: password,
                    tea_image: "avatar-icon.png"

                };
                con.query('INSERT INTO teacher SET ? ', Teacher, (err) => {
                    if (err) throw err;
                    res.redirect('/loginpagetea')
                });
            }
        });
    } else {
        res.redirect('/loginpagetea')
    }
});
//โชว์หน้าชั้นเรียน
app.get("/class", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query("SELECT * FROM class WHERE class.tea_id = ?", [teaid], (err, rows) => {
        if (err) console.log(err);
        return res.render(__dirname + "/html/" + "mainTea.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            Class: rows
        });
    })
});

//Add Class
app.post('/addClass', (req, res) => {
    const nameClass = req.body.cname;
    const classyear = req.body.cyear;
    console.log(teaid);
    const c = {
        class_name: nameClass,
        class_year: classyear,
        tea_id: teaid
    }
    con.query('INSERT INTO class SET ? ', c, (err) => {
        if (err) throw err;
    });
    return res.redirect('/class')
});

//โชว์หน้าเพิ่มชั้นเรียน
app.get("/pageaddclass", function (req, res) {
    res.sendFile(__dirname + "/html/" + "add-class-page.html");
});
app.get("/cancleClass", function (req, res) {
    res.redirect("/class");
});
//โชว์หน้าแก้ไขข้อมูลครู
app.get("/profile:id", function (req, res) {
    var { id } = req.params
    id_tc = id;
    console.log("ไอดีที่ต้องแก้ไข " + JSON.stringify(id_tc));
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [id_tc], (err, tears) => {
        if (err) console.log(err);
        // console.log(tears)
        return res.render(__dirname + "/html/" + "manage-teacher.html", {
            dt_name: tears[0].tea_firstname,
            dt_last: tears[0].tea_lastname,
            dt_user: tears[0].tea_username,
            dt_pw: tears[0].tea_password,
            dt_im: tears[0].tea_image
        });
    });
});

//แก้ไขข้อมูลครู
app.post("/editProfile", uploadfile.single("fileImg"), function (req, res) {
    if (req.file) {
        con.query("UPDATE teacher SET tea_firstname = '" + req.body.firstname + "',tea_lastname= '" + req.body.lastname + "',tea_username= '" + req.body.username + "',tea_password= '" + req.body.password + "',tea_image= '" + req.file.filename + "' WHERE tea_id = '" + id_tc + "'", (err, tea) => {
            if (err) throw err;
            console.log("อัปเดตข้อมูลครูสำเร็จ ไอดีที่ " + JSON.stringify(id_tc));
        });
    }
    res.redirect("/class");
});

//ยกเลิกการแก้ไขข้อมูลครู
app.get("/cancleEditTeacher", function (req, res) {
    res.redirect("/class");
});

//ออกจากระบบครู
app.get("/logouttea", function (req, res) {
    res.redirect("/loginpagetea");
});

//โชว์เนื้อหาบทเรียนในคลาส
app.get("/lesson:idclass", function (req, res) {
    var { idclass } = req.params
    id_class = idclass;
    console.log(id_class)
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })

    con.query('SELECT * FROM seclesson WHERE class_id = ? ', [id_class], (err, rows) => {
        if (err) console.log(err);
        con.query('SELECT * FROM lesson', (err, lesson) => {
            if (err) console.log(err);
            return res.render(__dirname + "/html/" + "lesson-page.html", {
                id_t: teaid,
                name: tea_name,
                lastname: tea_lastname,
                secLes: rows,
                Les: lesson
            });
        })

    })
});

//เมื่อกดปุ่มบทเรียน โชว์บทเรียนในคลาส
app.get("/lesson", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query('SELECT * FROM seclesson WHERE class_id = ? ', [id_class], (err, rows) => {
        if (err) console.log(err);
        con.query('SELECT * FROM lesson', (err, lesson) => {
            if (err) console.log(err);
            return res.render(__dirname + "/html/" + "lesson-page.html", {
                id_t: teaid,
                name: tea_name,
                lastname: tea_lastname,
                secLes: rows,
                Les: lesson
            });
        })
    })
});
//โชว์หน้าเพิ่มหัวข้อบทเรียน
app.get("/addseclesson", function (req, res) {
    res.sendFile(__dirname + "/html/" + "add-seclesson.html");
});

//เพิ่มหัวข้อบทเรียน
app.post("/addSecLessonOK", (req, res) => {
    const nameSec = req.body.seclessonname;
    var secLesson = {
        sec_name: nameSec,
        post_date: new Date(),
        class_id: id_class
    }
    con.query('INSERT INTO seclesson SET ? ', secLesson, (err) => {
        if (err) throw err;
        console.log("1 document secLesson")
    })
    res.redirect("/lesson");
});

app.get("/cancleSeclesson", function (req, res) {
    res.redirect("/lesson");
});

//โชว์บทเรียนตามหัวข้อ
app.get("/lesManage:idseclesson", function (req, res) {
    var { idseclesson } = req.params
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    });
    console.log(idseclesson)
    con.query('SELECT * FROM seclesson WHERE sec_lesson_id = ?', [idseclesson], (err, rs) => {
        if (err) throw err
        rs.forEach((st) => {
            secLesid = st.sec_lesson_id;
            secL = st.sec_name;
        });
    });
    con.query('SELECT * FROM lesson INNER JOIN seclesson ON lesson.sec_lesson_id = seclesson.sec_lesson_id WHERE seclesson.sec_lesson_id = ?', [idseclesson], (err, result) => {
        if (err) throw err
        console.log(result)
        return res.render(__dirname + "/html/" + "manage-lessons.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            secLes: secL,
            les: result
        });
    });
    id_seclesson = idseclesson
});

//โชว์บทเรียน
app.get("/lesManage", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    });
    console.log(id_seclesson);
    con.query('SELECT * FROM lesson INNER JOIN seclesson ON lesson.sec_lesson_id = seclesson.sec_lesson_id WHERE seclesson.sec_lesson_id = ?', [id_seclesson], (err, result) => {
        if (err) throw err
        console.log(result)
        return res.render(__dirname + "/html/" + "manage-lessons.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            secLes: result[0].sec_name,
            les: result
        });
    });
});

//โชว์หน้าสร้างบทเรียน
app.get("/creatLesson", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    return res.render(__dirname + "/html/" + "add-lesson.html", {
        id_t: teaid,
        name: tea_name,
        lastname: tea_lastname,
    });
})

//เพิ่มบทเรียน
app.post('/upload-multiple-pdf', uploadfile.fields([{ name: 'doc', maxCount: 2 }, { name: 'vdo', maxCount: 1 }]), (req, res) => {
    console.log(req.files);

    var lesson = {
        lesson_name: req.body.nameLesson,
        lesson_explain: req.body.expianLesson,
        lesson_type: req.body.lessontype,
        sec_lesson_id: id_seclesson
    }
    con.query('INSERT INTO lesson SET ? ', lesson, (err) => {
        if (err) throw err;
    });
    con.query('SELECT * FROM lesson ORDER BY lesson_id DESC LIMIT 1', (err, lesson) => {
        if (err) throw err;
        console.log(lesson);
        let fileslen = [].concat(req.files['doc']);
        console.log(fileslen.length)
        for (var i = 0; i < fileslen.length; i++) {
            var pdf = "INSERT INTO doc (doc_name, lesson_id) VALUES ('" + req.files.doc[i].filename + "','" + lesson[0].lesson_id + "')";
            con.query(pdf, function (err, result) {
                if (err) throw err;
                console.log("1 doc inserted");
            });
        }
        var vdo = "INSERT INTO vdo (vdo_name, lesson_id) VALUES ('" + req.files.vdo[0].filename + "','" + lesson[0].lesson_id + "')";
        con.query(vdo, function (err, result) {
            if (err) throw err;
            console.log("1 vdo inserted");
        });
    });
    res.redirect('/lesson');
});

//โชว์ข้อมูลที่หน้าแก้ไขบทเรียน
app.get("/editLes:id", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    var { id } = req.params
    console.log(id)
    con.query('SELECT * FROM lesson WHERE lesson_id = ?', [id], (err, reslesson) => {
        if (err) throw err
        console.log(reslesson)
        id_lesson = reslesson[0].lesson_id
        name_lesson = reslesson[0].lesson_name,
            expian_lesson = reslesson[0].lesson_explain
    })
    con.query('SELECT * FROM doc WHERE lesson_id = ?', [id], (err, doclesson) => {
        if (err) throw err
        con.query('SELECT * FROM vdo WHERE lesson_id = ?', [id], (err, vdolesson) => {
            if (err) throw err
            return res.render(__dirname + "/html/" + "edit-lesson.html", {
                id_t: teaid,
                name: tea_name,
                lastname: tea_lastname,
                detail_name: name_lesson,
                detail_expian: expian_lesson,
                doclist: doclesson,
                vdolist: vdolesson
            })
        })
    })

})

// update บทเรียน จากหน้าแก้ไข
app.post('/updateLesson', uploadfile.fields([{ name: 'doc', maxCount: 2 }, { name: 'vdo', maxCount: 1 }]), (req, res) => {
    //update ส่วนนหัวข้อบทเรียนและคำอธิบาย
    con.query("UPDATE lesson SET lesson_name = '" + req.body.nameLesson + "',lesson_explain= '" + req.body.expianLesson + "' WHERE lesson_id = '" + id_lesson + "'", (err, results) => {
        if (err) throw err;
    });
    if (req.files) {
        if (!req.files['doc'] == 0 && !req.files['vdo'] == 0) {
            //update ส่วนนไฟล์เอกสาร และวิดิโอ
            let fileslen = [].concat(req.files['doc']);
            console.log(fileslen.length)
            for (var i = 0; i < fileslen.length; i++) {
                //ตรวจสอบชื่อไฟล์เอกสาร
                console.log(req.files.doc[i].filename)
                var fileEditDoc = req.files.doc[i].filename;
                con.query('SELECT COUNT(*) AS countdoc FROM doc WHERE lesson_id = ?', [id_lesson], (err, rows) => {
                    if (rows[0].countdoc <= 1) {
                        var pdf = "INSERT INTO doc (doc_name, lesson_id) VALUES ('" + fileEditDoc + "','" + id_lesson + "')";
                        con.query(pdf, function (err, result) {
                            if (err) throw err;
                            console.log("1 doc inserted" + result);
                        });
                    }
                })

            }
            console.log(req.files.vdo[0].filename);
            var fileEdit = req.files.vdo[0].filename
            con.query('SELECT * FROM vdo WHERE lesson_id = ?', [id_lesson], (err, rowsVDO) => {
                console.log(rowsVDO);
                if (rowsVDO == []) {
                    var vdo = "INSERT INTO vdo (vdo_name, lesson_id) VALUES ('" + fileEdit + "','" + id_lesson + "')";
                    con.query(vdo, function (err, result) {
                        if (err) throw err;
                        console.log("1 vdo inserted" + result);
                    });
                }
            })
        } else if (!req.files['doc'] == 0) {
            let fileslen = [].concat(req.files['doc']);
            //console.log(fileslen.length)
            for (var i = 0; i < fileslen.length; i++) {
                //ตรวจสอบชื่อไฟล์เอกสาร
                console.log(req.files.doc[i].filename)
                var fileEditDoc = req.files.doc[i].filename;
                con.query('SELECT COUNT(*) AS countdoc FROM doc WHERE lesson_id = ?', [id_lesson], (err, rows) => {
                    console.log('countdoc ' + rows[0].countdoc);
                    if (rows[0].countdoc <= 1) {
                        var pdf = "INSERT INTO doc (doc_name, lesson_id) VALUES ('" + fileEditDoc + "','" + id_lesson + "')";
                        con.query(pdf, function (err, result) {
                            if (err) throw err;
                            console.log("1 doc inserted" + result);
                        });
                    }
                })

            }
        } else if (!req.files['vdo'] == 0) {
            console.log(req.files.vdo[0].filename);
            var fileEdit = req.files.vdo[0].filename
            con.query('SELECT * FROM vdo WHERE lesson_id = ?', [id_lesson], (err, rowsVDO) => {
                console.log(rowsVDO);
                if (rowsVDO == []) {
                    var vdo = "INSERT INTO vdo (vdo_name, lesson_id) VALUES ('" + fileEdit + "','" + id_lesson + "')";
                    con.query(vdo, function (err, result) {
                        if (err) throw err;
                        console.log("1 vdo inserted" + result);
                    });
                }
            })

        }

    }

    res.redirect('/lesManage');
});

//ลบไฟล์วิดิโอ จากหน้าแก้ไข
app.get("/deletefileVideo:id", function (req, res) {
    var { id } = req.params
    var lessoneditvdo;
    console.log(id)
    con.query('SELECT * FROM vdo WHERE vdo_id = ?', [id], (err, rows_vdo) => {
        if (err) console.log(err);
        console.log(rows_vdo)
        lessoneditvdo = JSON.stringify(rows_vdo[0].lesson_id).replace(/"(.*)"$/, '$1')
        filename = __dirname + '/vdo/' + rows_vdo[0].vdo_name
        //console.log(lessonedit)
        //console.log(filename)
        fs.unlink(filename, (err) => {
            if (err) {
                console.error(err)
                return
            }
        })
    })
    con.query('DELETE FROM vdo WHERE vdo_id=?', [id], (err, rows) => {
        if (err) console.log(err);
        console.log('delete 1 row' + rows);
        //console.log(lessonedit)
        return res.redirect('/editLes' + lessoneditvdo);
    })
});

//ลบไฟล์บทเรียน จากหน้าแก้ไข
app.get("/deletefileDoc:id", function (req, res) {
    var { id } = req.params
    var lessonedit;
    console.log(id)
    con.query('SELECT * FROM doc WHERE doc_id = ?', [id], (err, rows) => {
        if (err) console.log(err);
        console.log(rows)
        lessonedit = JSON.stringify(rows[0].lesson_id).replace(/"(.*)"$/, '$1')
        filename = __dirname + '/doc/' + rows[0].doc_name
        //console.log(lessonedit)
        //console.log(filename)
        fs.unlink(filename, (err) => {
            if (err) {
                console.error(err)
                return
            }
        })
    })
    con.query('DELETE FROM doc WHERE doc_id=?', [id], (err, rows) => {
        if (err) console.log(err);
        console.log('delete 1 row' + rows);
        //console.log(lessonedit)
        return res.redirect('/editLes' + lessonedit);
    })
});


//โชว์หัวข้อแบบทดสอบ
app.get("/test", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query('SELECT * FROM sectest WHERE class_id = ? ', [id_class], (err, rows) => {
        if (err) console.log(err);
        con.query('SELECT * FROM test', (err, test) => {
            if (err) console.log(err);
            return res.render(__dirname + "/html/" + "test-page.html", {
                id_t: teaid,
                name: tea_name,
                lastname: tea_lastname,
                secTest: rows,
                nameTest: test
            });
        })
    })
})

//โชว์หน้าเพิ่มหัวข้อแบบทดสอบ
app.get("/addSecTest", function (req, res) {
    res.sendFile(__dirname + "/html/" + "add-secTest.html");
})

//เพิ่มหัวข้อแบบทดสอบ
app.post("/addSecTestOK", (req, res) => {
    const nameSec = req.body.sectestname;
    var secTest = {
        sec_test_name: nameSec,
        post_date: new Date(),
        class_id: id_class
    }
    con.query('INSERT INTO sectest SET ? ', secTest, (err) => {
        if (err) throw err;
        console.log("1 document secTest")
    })
    res.redirect("/test");
})

app.get("/cancleSecTest", function (req, res) {
    res.redirect("/test");
})

//โชว์แบบทดสอบตามหัวข้อ
app.get("/testManage:id", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    var { id } = req.params
    id_sectest = id;
    //console.log(id_sectest)
    con.query('SELECT * FROM sectest WHERE sec_test_id = ?', [id], (err, rs) => {
        if (err) throw err
        rs.forEach((st) => {
            secTestid = st.sec_test_id;
            secT = st.sec_test_name;
        })
    })
    con.query('SELECT * FROM test INNER JOIN sectest ON test.sec_test_id = sectest.sec_test_id WHERE sectest.sec_test_id = ?', [id], (err, result) => {
        if (err) throw err
        // console.log(result)
        var testSp = [];
        var test = [];
        for (var i = 0; i < result.length; i++) {
            if (result[i].test_type == "พูด") {
                testSp.push(result[i])
            } else {
                test.push(result[i])
            }
        }
        //console.log(test)
        console.log(testSp)
        return res.render(__dirname + "/html/" + "manage-test.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            secTest: secT,
            test: test,
            testSp: testSp,
            STID: secTestid
        })
    })
})

//แสดงหน้าสร้างแบบทดสอบ
app.get("/creatTest:id", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query('SELECT * FROM lesson INNER JOIN seclesson ON lesson.sec_lesson_id = seclesson.sec_lesson_id WHERE seclesson.class_id = ?', [id_class], (err, result) => {
        if (err) console.log(err);
        //console.log(id_seclesson)
        //console.log(id_class)
        //console.log(result)
        return res.render(__dirname + "/html/" + "add-test.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            nameLes: result
        })
    })
})

//แสดงหน้าสร้างแบบทดสอบพูด
app.get("/NewTestSpeech:id", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query('SELECT * FROM lesson INNER JOIN seclesson ON lesson.sec_lesson_id = seclesson.sec_lesson_id WHERE seclesson.class_id = ?', [id_class], (err, result) => {
        if (err) console.log(err);
        //console.log(id_seclesson)
        //console.log(id_class)
        //console.log(result)
        return res.render(__dirname + "/html/" + "add-testSpeech.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            nameLes: result
        })
    })
})

//สร้างแบบทดสอบ
app.post('/NewTest', uploadfile.single('filePDF'), function (req, res) {
    if (req.file) {
        const nameTest = req.body.nameTest;
        const testExpain = req.body.expianTest;
        const typeTest = req.body.typeTest;
        const doc = req.file.filename;
        const nameLes = req.body.nameLes;
        const formTest = req.body.LinkForm;
        console.log("Les ID Is " + nameLes)
        var test = {
            test_name: nameTest,
            test_explain: testExpain,
            test_type: typeTest,
            sec_test_id: id_sectest,
            lesson_id: nameLes,
            stu_id: "",
            test_doc: doc,
            test_form: formTest,
            test_score: req.body.scoreTest
        }
        console.log(test)
        con.query('INSERT INTO test SET ?', test, (err) => {
            if (err) console.log(err);
            console.log("1 document Test")
        })
        con.query('SELECT test_id FROM test WHERE test_id = (SELECT MAX(test_id) FROM test)', (err, rows) => {
            if (err) console.log(err);
            var sum = {
                score_min: 0,
                score_max: 0,
                score_mean: 0,
                test_id: rows[0].test_id
            }
            con.query('INSERT INTO scorestac SET ?', sum, (err) => {
                if (err) console.log(err);
                console.log('add scoreSum SP 1 insert')
            })
        })

    } else {
        const nameTest = req.body.nameTest;
        const testExpain = req.body.expianTest;
        const typeTest = req.body.typeTest;
        const nameLes = req.body.nameLes;
        const formTest = req.body.LinkForm;
        console.log("Les ID Is " + nameLes)
        var test = {
            test_name: nameTest,
            test_explain: testExpain,
            test_type: typeTest,
            sec_test_id: id_sectest,
            lesson_id: nameLes,
            stu_id: "",
            test_doc: '',
            test_form: formTest,
            test_score: req.body.scoreTest
        }
        console.log(test)
        con.query('INSERT INTO test SET ?', test, (err) => {
            if (err) console.log(err);
            console.log("1 document Test , No Doc ")
        })
        //---------
        con.query('SELECT test_id FROM test WHERE test_id = (SELECT MAX(test_id) FROM test)', (err, rows) => {
            if (err) console.log(err);
            var sum = {
                score_min: 0,
                score_max: 0,
                score_mean: 0,
                test_id: rows[0].test_id
            }
            con.query('INSERT INTO scorestac SET ?', sum, (err) => {
                if (err) console.log(err);
                console.log('add scoreSum SP 1 insert')
            })
        })
    }
    res.redirect('/TestManage');
})

//สร้างแบบทดสอบพูด
app.post('/addTestSpeech', function (req, res) {
    const nameTest = req.body.nameTest;
    const testExpain = req.body.expianTest;
    const nameLes = req.body.nameLes;
    const testVer = req.body.testSp;
    console.log("Les ID Is " + nameLes)
    var test = {
        test_name: nameTest,
        test_explain: testExpain,
        test_type: 'พูด',
        sec_test_id: id_sectest,
        lesson_id: nameLes,
        stu_id: "",
        test_doc: '',
        test_form: '',
        test_ver: testVer,
        test_score: req.body.scoreTest
    }
    console.log(test)
    con.query('INSERT INTO test SET ?', test, (err) => {
        if (err) console.log(err);
        console.log("1 document TestSpeech")
    })
    con.query('SELECT test_id FROM test WHERE test_id = (SELECT MAX(test_id) FROM test)', (err, rows) => {
        if (err) console.log(err);
        var sum = {
            score_min: 0,
            score_max: 0,
            score_mean: 0,
            test_id: rows[0].test_id
        }
        con.query('INSERT INTO scorestac SET ?', sum, (err) => {
            if (err) console.log(err);
            console.log('add scoreSum SP 1 insert')
        })
    })

    res.redirect('/TestManage');
})

//โชว์ข้อมูลที่หน้าแก้ไขแบบทดสอบ
app.get("/editTest:id", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    var { id } = req.params
    console.log(id)
    con.query('SELECT * FROM test WHERE test_id = ?', [id], (err, restest) => {
        if (err) throw err
        //console.log(restest)
        id_Test = restest[0].test_id
        //console.log(id_Test)
        return res.render(__dirname + "/html/" + "edit-test.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            detail_name: restest[0].test_name,
            detail_expian: restest[0].test_explain,
            dt_doc: restest[0].test_doc,
            dt_form: restest[0].test_form,
            dt_score: restest[0].test_score
        })
    })
})

// update แบบทดสอบ จากหน้าแก้ไข
app.post('/updateTest', uploadfile.single('filePDF'), (req, res) => {
    if (req.file) {
        con.query("UPDATE test SET test_name = '" + req.body.nameTest + "',test_explain= '" + req.body.expianTest + "',test_doc= '" + req.file.filename + "',test_form= '" + req.body.LinkForm + "',test_score= '" + req.body.scoreTest + "' WHERE test_id = '" + id_Test + "'", (err, results) => {
            if (err) throw err;
            console.log("1 update Test")
        });
    } else {
        con.query("UPDATE test SET test_name = '" + req.body.nameTest + "',test_explain= '" + req.body.expianTest + "',test_form= '" + req.body.LinkForm + "',test_score= '" + req.body.scoreTest + "' WHERE test_id = '" + id_Test + "'", (err, results) => {
            if (err) throw err;
            console.log("1 update Test, No Doc")
        });
    }
    res.redirect('/TestManage');
});

//โชว์ข้อมูลที่หน้าแก้ไขแบบทดสอบพูด
app.get("/showEditSpeech:id", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    var { id } = req.params
    console.log(id)
    con.query('SELECT * FROM test WHERE test_id = ?', [id], (err, restest) => {
        if (err) throw err
        //console.log(restest)
        id_Test = restest[0].test_id
        //console.log(id_Test)
        return res.render(__dirname + "/html/" + "edit-testSpeech.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            detail_name: restest[0].test_name,
            detail_expian: restest[0].test_explain,
            dt_ver: restest[0].test_ver,
            dt_score: restest[0].test_score
        })
    })
})

// update แบบทดสอบ จากหน้าแก้ไข
app.post('/editTestSpeech', (req, res) => {

    con.query("UPDATE test SET test_name = '" + req.body.nameTest + "',test_explain= '" + req.body.expianTest + "',test_ver= '" + req.body.testSp + "',test_score= '" + req.body.scoreTest + "' WHERE test_id = '" + id_Test + "'", (err, results) => {
        if (err) throw err;
        console.log("1 update TestSP")
    });

    res.redirect('/TestManage');
});

app.get("/TestManage", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    //console.log(id_sectest)
    con.query('SELECT * FROM test INNER JOIN sectest ON test.sec_test_id = sectest.sec_test_id WHERE sectest.sec_test_id = ?', [id_sectest], (err, result) => {
        if (err) throw err
        //console.log(result)
        var testSp = [];
        var test = [];
        for (var i = 0; i < result.length; i++) {
            if (result[i].test_type == "พูด") {
                testSp.push(result[i])
            } else {
                test.push(result[i])
            }
        }
        return res.render(__dirname + "/html/" + "manage-test.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            secTest: result[0].sec_test_name,
            test: test,
            testSp: testSp,
            STID: secTestid
        })
    })
})

//เพิ่มคะแนนเทสพูด
app.post("/scoreSpeech", function (req, res) {
    const sp = req.body.scoreSP
    con.query('SELECT * FROM `student` WHERE student.stu_number = ? AND student.class_id = ?', [stuNum, id_class], (err, result) => {
        if (err) console.log(err);
        if (result.length > 0) {
            id_stu = JSON.stringify(result[0].stu_id).replace(/"(.*)"$/, '$1')
        }
        //console.log('id นักเรียน : ' + id_stu)
        var score = {
            test_id: id_testSTU,
            stu_id: id_stu,
            score: sp,
            class_id: id_class
        }
        //console.log(score)
        con.query('INSERT INTO score SET ?', score, (err) => {
            if (err) console.log(err);
            console.log('add score SP 1 insert')
        })
        con.query('SELECT * FROM `scorestac` WHERE test_id = ?', [id_testSTU], (err, scoreSum) => {
            if (err) console.log(err);
            //console.log(scoreSum)
            if (scoreSum.length > 0) {
                con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [id_testSTU], (err, scoreSP) => {
                    con.query("UPDATE scorestac SET score_min = '" + scoreSP[0].Min + "',score_max= '" + scoreSP[0].Max + "',score_mean= '" + scoreSP[0].Mean + "' WHERE test_id = '" + id_testSTU + "'", (err) => {
                        if (err) console.log(err);
                        console.log('edit scoreSum SP 1 insert')
                        //res.redirect('/AllTestSut' + id_sectest);
                    })
                })
                con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [id_stu, id_class], (err, sumStu) => {
                    if (err) console.log(err);
                    console.log(sumStu)
                    console.log('id นักเรียน : ' + id_stu)
                    con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [id_stu], (err, allSum) => {
                        if (err) console.log(err);
                        if (allSum.length > 0) {
                            con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + id_stu + "'", (err) => {
                                if (err) console.log(err);
                                console.log('edit sumscorestu by stu_id : ' + id_stu)
                                res.redirect('/AllTestSut' + id_sectest);
                            })
                        } else {
                            var newSumStu = {
                                stu_id: sumStu[0].stu_id,
                                name_stu: sumStu[0].stu_name,
                                last_stu: sumStu[0].stu_lastname,
                                sumscore: sumStu[0].Sum,
                                class_id: sumStu[0].class_id
                            }
                            con.query('INSERT INTO sumscorestu SET ?', newSumStu, (err) => {
                                if (err) console.log(err);
                                console.log('add sumscorestu by stu_id : ' + id_stu)
                                res.redirect('/AllTestSut' + id_sectest);
                            })
                        }
                    })
                })
            } else {
                con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [id_testSTU], (err, scoreSP) => {
                    var sum = {
                        score_min: scoreSP[0].Min,
                        score_max: scoreSP[0].Max,
                        score_mean: scoreSP[0].Mean,
                        test_id: scoreSP[0].test_id
                    }
                    con.query('INSERT INTO scorestac SET ?', sum, (err) => {
                        if (err) console.log(err);
                        console.log('add scoreSum SP 1 insert')
                    })
                })
                con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [id_stu, id_class], (err, sumStu) => {
                    if (err) console.log(err);
                    console.log(sumStu)
                    console.log('id นักเรียน : ' + id_stu)
                    con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [id_stu], (err, allSum) => {
                        if (err) console.log(err);
                        if (allSum.length > 0) {
                            con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + id_stu + "'", (err) => {
                                if (err) console.log(err);
                                console.log('edit sumscorestu by stu_id : ' + id_stu)
                                res.redirect('/AllTestSut' + id_sectest);
                            })
                        } else {
                            var newSumStu = {
                                stu_id: sumStu[0].stu_id,
                                name_stu: sumStu[0].stu_name,
                                last_stu: sumStu[0].stu_lastname,
                                sumscore: sumStu[0].Sum,
                                class_id: sumStu[0].class_id
                            }
                            con.query('INSERT INTO sumscorestu SET ?', newSumStu, (err) => {
                                if (err) console.log(err);
                                console.log('add sumscorestu by stu_id : ' + id_stu)
                                res.redirect('/AllTestSut' + id_sectest);
                            })
                        }
                    })
                })
            }
        })
    })
})

//โชว์ตารางคะแนนสถิติ
app.get("/score", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query('SELECT * FROM `scorestac` JOIN test ON scorestac.test_id = test.test_id JOIN sectest ON sectest.sec_test_id = test.sec_test_id WHERE sectest.class_id = ? ', [id_class], (err, scoreStac) => {
        if (err) console.log(err);
        con.query('SELECT test.test_id,test.test_name, ( SELECT COUNT(score.stu_id) FROM score WHERE score.test_id = test.test_id ORDER BY score DESC ) as sum FROM test JOIN sectest ON test.sec_test_id = sectest.sec_test_id WHERE sectest.class_id = ? GROUP BY test.test_id ORDER BY test_id ASC', [id_class], (err, sumScore) => {
            return res.render(__dirname + "/html/" + "scoreTea-page.html", {
                id_t: teaid,
                name: tea_name,
                lastname: tea_lastname,
                sumScore: sumScore,
                scoreStac: scoreStac,
            })
        })
    })
})

//หน้าโชว์รายการคะแนนคะแนนแต่ละเทส
app.get("/viewScore:id", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    var { id } = req.params
    id_view_score = id
    console.log(id_view_score)
    con.query('SELECT * FROM `test` WHERE test.test_id = ?', [id_view_score], (err, test) => {
        if (err) console.log(err);
        test.forEach((row) => {
            testid = row.test_id;
            testName = row.test_name;
        });
    })
    //console.log(id_view_score)
    con.query('SELECT * FROM score JOIN student ON score.stu_id = student.stu_id JOIN test ON score.test_id = test.test_id WHERE score.test_id = ?', [id_view_score], (err, Scorelist) => {
        if (err) throw err
        //console.log(Scorelist)
        return res.render(__dirname + "/html/" + "scoreTeadata-page.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            Scorelist: Scorelist,
            testName: testName,
            testid: testid
        })
    })

})

//หน้าโชว์ฟอร์ม เพิ่มคะแนนแต่ะละเทส
app.get("/addScore:id", function (req, res) {
    var { id } = req.params
    console.log(id)
    con.query('SELECT * FROM test  WHERE test_id = ?', [id], (err, rows) => {
        if (err) console.log(err);
        return res.render(__dirname + "/html/" + "add-score-page.html", {
            testid: rows[0].test_id
        })

    })
})

//ส่งข้อมูลจาก หน้าฟอร์ม เพิ่มคะแนน เข้า ดาต้า
app.post("/addScore:idtest", function (req, res) {
    var { idtest } = req.params
    var classidAdd, scoreNew;
    studentNew = req.body.studentID
    scoreNew = req.body.score
    con.query('SELECT * FROM test JOIN sectest ON test.sec_test_id = sectest.sec_test_id WHERE test_id = ?', [idtest], (err, rows) => {
        if (err) console.log(err);
        classidAdd = rows[0].class_id
    })
    con.query('SELECT * FROM student  WHERE stu_number = ?', [studentNew], (err, result) => {
        if (err) console.log(err);
        var newScore = {
            test_id: idtest,
            stu_id: result[0].stu_id,
            score: scoreNew,
            class_id: classidAdd,

        }
        con.query('INSERT INTO score SET ?', newScore, (err, newscorestudent) => {
            if (err) console.log(err);
            console.log('add newScore by stu_id : ' + newscorestudent)
        })

        con.query('SELECT * FROM `scorestac` WHERE test_id = ?', [idtest], (err, scoreSum) => { //ค้นหาว่ามีข้อมูลหรือยัง
            if (err) console.log(err);
            //console.log(scoreSum)
            if (scoreSum.length > 0) { //ถ้ามี อัตเดตข้อมูลในตาราง สถิติคะแนนแต่ละแบบทดสอบ และ ตารางคะแนนรวมของนักเรียน
                con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [idtest], (err, scoreSP) => {
                    con.query("UPDATE scorestac SET score_min = '" + scoreSP[0].Min + "',score_max= '" + scoreSP[0].Max + "',score_mean= '" + scoreSP[0].Mean + "' WHERE test_id = '" + scoreSP[0].test_id + "'", (err) => {
                        if (err) console.log(err);
                        console.log('update scoreStac SP 1 insert')
                    })
                })
                con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [result[0].stu_id, id_class], (err, sumStu) => {
                    if (err) console.log(err);
                    console.log(sumStu)
                    console.log('id นักเรียน : ' + result[0].stu_id)
                    con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [result[0].stu_id], (err, allSum) => {
                        if (err) console.log(err);
                        if (allSum.length > 0) {
                            con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + result[0].stu_id + "'", (err) => {
                                if (err) console.log(err);
                                console.log('update sumscorestu by stu_id : ' + result[0].stu_id)
                                return res.redirect('/viewScore' + idtest);
                            })
                        } else {
                            var newSumStu = {
                                stu_id: sumStu[0].stu_id,
                                name_stu: sumStu[0].stu_name,
                                last_stu: sumStu[0].stu_lastname,
                                sumscore: sumStu[0].Sum,
                                class_id: sumStu[0].class_id
                            }
                            con.query('INSERT INTO sumscorestu SET ?', newSumStu, (err) => {
                                if (err) console.log(err);
                                console.log('add sumscorestu by stu_id : ' + id_stu)
                                return res.redirect('/viewScore' + idtest);
                            })
                        }
                    })
                })
            } else {
                con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [idtest], (err, scoreSP) => {
                    var sum = {
                        score_min: scoreSP[0].Min,
                        score_max: scoreSP[0].Max,
                        score_mean: scoreSP[0].Mean,
                        test_id: scoreSP[0].test_id
                    }
                    con.query('INSERT INTO scorestac SET ?', sum, (err) => {
                        if (err) console.log(err);
                        console.log('add scoreStacSP 1 insert')
                    })
                })
                con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [result[0].stu_id, id_class], (err, sumStu) => {
                    if (err) console.log(err);
                    console.log(sumStu)
                    console.log('id นักเรียน : ' + result[0].stu_id)
                    con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [result[0].stu_id], (err, allSum) => {
                        if (err) console.log(err);
                        if (allSum.length > 0) {
                            con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + result[0].stu_id + "'", (err) => {
                                if (err) console.log(err);
                                console.log('edit sumscorestu by stu_id : ' + result[0].stu_id)
                                return res.redirect('/viewScore' + idtest);
                            })
                        } else {
                            var newSumStu = {
                                stu_id: sumStu[0].stu_id,
                                name_stu: sumStu[0].stu_name,
                                last_stu: sumStu[0].stu_lastname,
                                sumscore: sumStu[0].Sum,
                                class_id: sumStu[0].class_id
                            }
                            con.query('INSERT INTO sumscorestu SET ?', newSumStu, (err) => {
                                if (err) console.log(err);
                                console.log('add sumscorestu by stu_id : ' + id_stu)
                                return res.redirect('/viewScore' + idtest);
                            })
                        }
                    })
                })
            }
        })
    });
})
app.get("/cancleScore", function (req, res) {
    return res.redirect("/viewScore" + id_view_score);
})

//ส่งข้อมูลจาก หน้าฟอร์มเพิ่มคะแนน file CSV เข้าดาต้า
app.post('/uploadfileScore:idtest', uploadfile.single("uploadfileScore"), (req, res) => {
    var { idtest } = req.params
    var classidAdd
    id_testSTU = idtest
    con.query('SELECT * FROM test JOIN sectest ON test.sec_test_id = sectest.sec_test_id WHERE test_id = ?', [idtest], (err, rows) => {
        if (err) console.log(err);
        classidAdd = rows[0].class_id
    })
    // CSV file name
    //const fileName = __dirname + '/uploads/' + req.file.filename;
    importExcelData2MySQL(__dirname + '/uploads/' + req.file.filename);

    function importExcelData2MySQL(fileName) {
        //readXlsxFile(fileName).then(source => {
        //const fileName = __dirname + '/uploads/' + req.file.filename;

        csvtojson().fromFile(fileName).then(source => {
            //console.log("row " + source.values);
            for (var i = 0; i < source.length; i++) {
                var student_num = source[i]["รหัสนักเรียน"]
                var student_score = source[i]["คะแนน"]
                var io = 0;
                var stu_score;
                for (var j = 0; j < student_score.length; j++) {
                    if (student_score[j] == '/') {
                        stu_score = student_score.split("/")
                    } else {
                        score = student_score;
                    }
                }
                console.log(student_num);
                console.log(student_score);
                //console.log(stu_score[0]);
                var score = stu_score[0]
                console.log(score);
                nextStep3(student_num, score);

                function nextStep3(student_num, score) {
                    con.query('SELECT * FROM student  WHERE stu_number = ? And class_id = ?', [student_num, id_class], (err, result) => {
                        if (err) console.log(err);
                        idstudent_score = JSON.stringify(result[0].stu_id).replace(/"(.*)"$/, '$1');
                        console.log(stu_score[0])
                        nextStep2(idstudent_score, score);
                    });
                }

                function nextStep2(idstudent_score, score) {
                    nextStep(idstudent_score, score);
                }

                function nextStep(idstudent_score, score) {
                    console.log("id Student Score " + idstudent_score)
                    console.log("Student Score " + score)
                    var test_id = idtest,
                        stu_id = idstudent_score,
                        score = score,
                        class_id = classidAdd

                    var insertStatement =
                        `INSERT INTO score (test_id,stu_id,score, class_id) values(?, ?, ?, ?)`;
                    var items = [test_id, stu_id, score, class_id];

                    // Inserting data of current row
                    // into database
                    con.query(insertStatement, items,
                        (err, results, fields) => {
                            if (err) {
                                console.log(
                                    "Unable to insert item at row " + i + 1);
                                return console.log(err);
                            }
                        });

                    con.query('SELECT * FROM `scorestac` WHERE test_id = ?', [idtest], (err, scoreSum) => {
                        if (err) console.log(err);
                        io++;
                        //console.log(scoreSum)
                        if (scoreSum.length > 0) {
                            con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [idtest], (err, scoreSP) => {
                                con.query("UPDATE scorestac SET score_min = '" + scoreSP[0].Min + "',score_max= '" + scoreSP[0].Max + "',score_mean= '" + scoreSP[0].Mean + "' WHERE test_id = '" + id_testSTU + "'", (err) => {
                                    if (err) console.log(err);
                                    console.log('edit scoreSum SP 1 insert')
                                    //res.redirect('/AllTestSut' + id_sectest);
                                })
                            })
                            con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [idstudent_score, id_class], (err, sumStu) => {
                                if (err) console.log(err);
                                console.log(sumStu)
                                console.log('id นักเรียน : ' + idstudent_score)
                                con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [idstudent_score], (err, allSum) => {
                                    if (err) console.log(err);
                                    if (allSum.length > 0) {
                                        con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + idstudent_score + "'", (err) => {
                                            if (err) console.log(err);
                                            console.log('edit sumscorestu by stu_id : ' + idstudent_score)
                                        })
                                    } else {
                                        var newSumStu = {
                                            stu_id: sumStu[0].stu_id,
                                            name_stu: sumStu[0].stu_name,
                                            last_stu: sumStu[0].stu_lastname,
                                            sumscore: sumStu[0].Sum,
                                            class_id: sumStu[0].class_id
                                        }
                                        con.query('INSERT INTO sumscorestu SET ?', newSumStu, (err) => {
                                            if (err) console.log(err);
                                            console.log('add sumscorestu by stu_id : ' + idstudent_score)
                                        })
                                    }
                                })
                            })
                        } else {
                            console.log('ค่าของ io : ' + io)
                            if (io == 1) {
                                con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [idtest], (err, scoreSP) => {
                                    var sum = {
                                        score_min: scoreSP[0].Min,
                                        score_max: scoreSP[0].Max,
                                        score_mean: scoreSP[0].Mean,
                                        test_id: scoreSP[0].test_id
                                    }
                                    con.query('INSERT INTO scorestac SET ?', sum, (err) => {
                                        if (err) console.log(err);
                                        console.log('add scoreSum SP 1 insert')
                                    })
                                })
                                con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [idstudent_score, id_class], (err, sumStu) => {
                                    if (err) console.log(err);
                                    console.log(sumStu)
                                    console.log('id นักเรียน : ' + idstudent_score)
                                    con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [idstudent_score], (err, allSum) => {
                                        if (err) console.log(err);
                                        if (allSum.length > 0) {
                                            con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + idstudent_score + "'", (err) => {
                                                if (err) console.log(err);
                                                console.log('edit sumscorestu by stu_id : ' + idstudent_score)
                                            })
                                        } else {
                                            var newSumStu = {
                                                stu_id: sumStu[0].stu_id,
                                                name_stu: sumStu[0].stu_name,
                                                last_stu: sumStu[0].stu_lastname,
                                                sumscore: sumStu[0].Sum,
                                                class_id: sumStu[0].class_id
                                            }
                                            con.query('INSERT INTO sumscorestu SET ?', newSumStu, (err) => {
                                                if (err) console.log(err);
                                                console.log('add sumscorestu by stu_id : ' + idstudent_score)
                                            })
                                        }
                                    })
                                })
                            } else {
                                con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [idtest], (err, scoreSP) => {
                                    con.query("UPDATE scorestac SET score_min = '" + scoreSP[0].Min + "',score_max= '" + scoreSP[0].Max + "',score_mean= '" + scoreSP[0].Mean + "' WHERE test_id = '" + id_testSTU + "'", (err) => {
                                        if (err) console.log(err);
                                        console.log('edit scoreSum SP 1 insert')
                                        //res.redirect('/AllTestSut' + id_sectest);
                                    })
                                })
                                con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [idstudent_score, id_class], (err, sumStu) => {
                                    if (err) console.log(err);
                                    console.log(sumStu)
                                    console.log('id นักเรียน : ' + idstudent_score)
                                    con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [idstudent_score], (err, allSum) => {
                                        if (err) console.log(err);
                                        if (allSum.length > 0) {
                                            con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + idstudent_score + "'", (err) => {
                                                if (err) console.log(err);
                                                console.log('edit sumscorestu by stu_id : ' + idstudent_score)
                                            })
                                        } else {
                                            var newSumStu = {
                                                stu_id: sumStu[0].stu_id,
                                                name_stu: sumStu[0].stu_name,
                                                last_stu: sumStu[0].stu_lastname,
                                                sumscore: sumStu[0].Sum,
                                                class_id: sumStu[0].class_id
                                            }
                                            con.query('INSERT INTO sumscorestu SET ?', newSumStu, (err) => {
                                                if (err) console.log(err);
                                                console.log('add sumscorestu by stu_id : ' + idstudent_score)
                                            })
                                        }
                                    })
                                })
                            }
                        }
                    })
                }
            }
            fs.unlinkSync(fileName)
            console.log("All Score student into database successfully");
            return res.redirect("/viewScore" + id_view_score);
        });
    }
});

//โชว์หน้าแก้ไขคะแนน
app.get("/editScore:idscore", function (req, res) {
    var { idscore } = req.params
    con.query("SELECT * FROM score JOIN student ON score.stu_id = student.stu_id WHERE score_id = ?", [idscore], (err, result) => {
        if (err) throw err;
        return res.render(__dirname + "/html/edit-score-page.html", {
            userscore: result[0]
        });
    });
})

//อัพเดตคะแนนนักเรียน
app.post('/updateScore', (req, res) => {
    console.log(req.body.scoreId)
    con.query("UPDATE score SET score = '" + req.body.score + "' WHERE score_id = '" + req.body.scoreId + "'", (err, results) => {
        if (err) throw err;
        console.log('edit score by score_id : ' + req.body.scoreId)
    });
    con.query('SELECT * FROM score WHERE score_id = ?', [req.body.scoreId], (err, rowsresults) => {
        if (err) console.log(err);
        id_testSTU = rowsresults[0].test_id;
        id_stu = rowsresults[0].stu_id;
        con.query('SELECT * FROM `scorestac` WHERE test_id = ?', [id_testSTU], (err, scoreSum) => {
            if (err) console.log(err);
            //console.log(scoreSum)
            con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [id_testSTU], (err, scoreSP) => {
                con.query("UPDATE scorestac SET score_min = '" + scoreSP[0].Min + "',score_max= '" + scoreSP[0].Max + "',score_mean= '" + scoreSP[0].Mean + "' WHERE test_id = '" + id_testSTU + "'", (err) => {
                    if (err) console.log(err);
                    console.log('edit scoreSum SP 1 insert')
                    //res.redirect('/AllTestSut' + id_sectest);
                })
            })
            con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [id_stu, id_class], (err, sumStu) => {
                if (err) console.log(err);
                console.log(sumStu)
                console.log('id นักเรียน : ' + id_stu)
                con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [id_stu], (err, allSum) => {
                    if (err) console.log(err);
                    con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + id_stu + "'", (err) => {
                        if (err) console.log(err);
                        console.log('edit sumscorestu by stu_id : ' + id_stu)
                        res.redirect('/viewScore' + id_testSTU);
                    })
                })
            })
        })
    })
});
//ปุ่มยกเลิกหน้าแก้ไขคะแนน
app.get("/cancleScore:id_view_score", function (req, res) {
    var { id_view_score } = req.params
    res.redirect("/viewScore" + id_view_score);
})

//ลบรายการคะแนน
app.get("/DelScore:id", function (req, res) {
    var { id } = req.params
    con.query('SELECT * FROM `score` WHERE `score_id` = ?', [id], (err, deltest) => {
        if (err) console.log(err);
        id_testSTU = deltest[0].test_id;
        id_stu = deltest[0].stu_id;
        con.query('DELETE FROM score WHERE score_id=?', [id], (err, rows) => {
            if (err) console.log(err);
            console.log('delete 1 row' + rows);
            //console.log(lessonedit)
            //return res.redirect('/viewScore' + id_testSTU);
        })
        con.query('SELECT * FROM `scorestac` WHERE test_id = ?', [id_testSTU], (err, scoreSum) => {
            if (err) console.log(err);
            //console.log(scoreSum)
            con.query('SELECT MIN(score) AS Min, MAX(score) AS Max, AVG(Cast(score as float)) AS Mean , test_id FROM score WHERE test_id = ?', [id_testSTU], (err, scoreSP) => {
                con.query("UPDATE scorestac SET score_min = '" + scoreSP[0].Min + "',score_max= '" + scoreSP[0].Max + "',score_mean= '" + scoreSP[0].Mean + "' WHERE test_id = '" + id_testSTU + "'", (err) => {
                    if (err) console.log(err);
                    console.log('edit scoreSum SP 1 insert')
                    //res.redirect('/AllTestSut' + id_sectest);
                })
            })
            con.query('SELECT *, ( SELECT sum(score) FROM score WHERE score.stu_id = student.stu_id ORDER BY score DESC ) as Sum FROM student WHERE student.stu_id = ? AND student.class_id = ? GROUP BY student.stu_id ORDER BY sum DESC', [id_stu, id_class], (err, sumStu) => {
                if (err) console.log(err);
                console.log(sumStu)
                console.log('id นักเรียน : ' + id_stu)
                if (sumStu > 0) {
                    con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [id_stu], (err, allSum) => {
                        if (err) console.log(err);
                        con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + id_stu + "'", (err) => {
                            if (err) console.log(err);
                            console.log('edit sumscorestu by stu_id : ' + id_stu)
                            res.redirect('/viewScore' + id_testSTU);
                        })
                    })
                } else {
                    con.query('SELECT * FROM sumscorestu WHERE stu_id = ?', [id_stu], (err, allSum) => {
                        if (err) console.log(err);
                        con.query("UPDATE sumscorestu SET sumscore = '" + sumStu[0].Sum + "'WHERE stu_id = '" + id_stu + "'", (err) => {
                            if (err) console.log(err);
                            console.log('edit sumscorestu by stu_id : ' + id_stu)
                            res.redirect('/viewScore' + id_testSTU);
                        })
                    })
                }
            })
        })
    })
})

// โชว์สถิติการเข้าเรียน
app.get("/statistics", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query('SELECT * FROM statisticssum JOIN lesson ON statisticssum.lesson_id = lesson.lesson_id JOIN seclesson ON lesson.sec_lesson_id = seclesson.sec_lesson_id AND seclesson.class_id = ?', [id_class], (err, rows) => {
        if (err) console.log(err);
        return res.render(__dirname + "/html/" + "statistics-page.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            stc: rows,
        })
    })
})

//โชว์รายชื่อนักเรียน
app.get("/student", function (req, res) {
    console.log(id_class)
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query("SELECT * FROM student WHERE class_id =?", [id_class], (err, rows) => {
        if (err) console.log(err);
        return res.render(__dirname + "/html/" + "student-page.html", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            stu: rows,
            classStu: id_class
        });
    })
})

//โชว์หน้าฟร์อมเพิ่มนักเรียน
app.get("/addStudent:idClass", function (req, res) {
    var { idClass } = req.params
    con.query('SELECT * FROM class  WHERE class_id = ?', [idClass], (err, rows) => {
        return res.render(__dirname + "/html/" + "add-student-page.html", {
            classStu: rows[0].class_name
        });
    });
})
app.get("/cancleStudent", function (req, res) {
    res.redirect("/student");
})

//เพิ่มนักเรียนรายคน
app.post('/addStu', (req, res) => {
    const name = req.body.firstname;
    const lname = req.body.lastname;
    const user = req.body.studentID;
    const password = req.body.studentID;
    const classname = req.body.className;
    //console.log(id_class);
    var Student = {
        stu_name: name,
        stu_lastname: lname,
        stu_number: user,
        stu_password: password,
        stu_classname: classname,
        class_id: id_class
    }
    con.query('INSERT INTO student SET ? ', Student, (err) => {
        if (err) throw err;
    });
    return res.redirect('/student')
});

//เพิ่มนักเรียนหลายคน
// upload csv to database
app.post('/uploadfile', uploadfile.single("fileStu"), (req, res) => {
    // CSV file name
    const fileName = __dirname + '/uploads/' + req.file.filename;

    csvtojson().fromFile(fileName).then(source => {

        // Fetching the data from each row 
        // and inserting to the table "sample"
        for (var i = 0; i < source.length; i++) {
            var stu_name = source[i]["Name"],
                stu_lastname = source[i]["Lastname"],
                stu_number = source[i]["Number"],
                stu_classname = source[i]["Classname"]

            var insertStatement =
                `INSERT INTO student (stu_name, stu_lastname, stu_number, stu_password, stu_classname,class_id) values(?, ?, ?, ?, ?,?)`;
            var items = [stu_name, stu_lastname, stu_number, stu_number, stu_classname, id_class];

            // Inserting data of current row
            // into database
            con.query(insertStatement, items,
                (err, results, fields) => {
                    if (err) {
                        console.log(
                            "Unable to insert item at row ", i + 1);
                        return console.log(err);
                    }
                });
        }
        fs.unlinkSync(fileName)
        console.log(
            "All user student into database successfully");
    });
    console.log('CSV file data has been uploaded in mysql database ');
    res.redirect("/student");
});


//โชว์หน้าแก้ไขนักเรียน
app.get("/editStu:idstu", function (req, res) {
    var { idstu } = req.params
    id_student = idstu;
    con.query("SELECT stu_name, stu_lastname, stu_number, stu_password , stu_classname FROM student WHERE stu_id = ?  ", [id_student], (err, result) => {
        if (err) throw err;
        return res.render(__dirname + "/html/editStu.html", {
            user: result[0]
        });
    });
})

//แก้ไขข้อมูลนักเรียน
app.post('/updateStu', (req, res) => {
    con.query("UPDATE student SET stu_name = '" + req.body.firstname + "',stu_lastname= '" + req.body.lastname + "',stu_number= '" + req.body.studentID + "',stu_classname= '" + req.body.className + "' WHERE stu_id = '" + id_student + "'", (err, results) => {
        if (err) throw err;
        console.log('update Stu 1 row')
    });
    con.query("UPDATE sumscorestu SET name_stu = '" + req.body.firstname + "',last_stu= '" + req.body.lastname + "' WHERE stu_id = '" + id_student + "'", (err, re) => {
        if (err) throw err;
        console.log('update name in sumscorestu 1 row')
    });
    res.redirect('/student');
});

//ลบรายชื่อนักเรียน
app.get("/DelStu:id", function (req, res) {
    var { id } = req.params
    con.query('SELECT * FROM student WHERE stu_id = ?', [id], (err, delStu) => {
        if (err) console.log(err);
        id_class = delStu[0].class_id;
        id_stu = delStu[0].stu_id;
        con.query('DELETE FROM student WHERE stu_id=?', [id], (err, rows) => {
            if (err) console.log(err);
            console.log('delete 1 row' + rows);
            //console.log(lessonedit)
            return res.redirect('/student');
        })

    })
})

//แดชบอร์ดรายงานผลนักเรียน
app.get("/dashboard", function (req, res) {
    con.query('SELECT * FROM teacher  WHERE teacher.tea_id = ?', [teaid], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            teaid = row.tea_id;
            tea_name = row.tea_firstname;
            tea_lastname = row.tea_lastname;
        });
    })
    con.query('SELECT SUM(test_score) AS scoreSumTest FROM test JOIN sectest ON test.`sec_test_id` = sectest.sec_test_id WHERE sectest.class_id = ?', [id_class], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            scoreTestSum = row.scoreSumTest;
        });
    })
    con.query('SELECT * , (SELECT SUM(statistics.stac_num) FROM statistics WHERE sumscorestu.stu_id = statistics.stu_id) as actention FROM sumscorestu WHERE class_id = ?', [id_class], (err, report) => {
        if (err) console.log(err);
        return res.render(__dirname + "/html/" + "dashboard-page", {
            id_t: teaid,
            name: tea_name,
            lastname: tea_lastname,
            report: report,
            report_scoreTest: scoreTestSum
        })
    })

})

//Login นักเรียน
app.post("/loginstu", function (req, res) {
    var username = req.body.usernameStu
    var password = req.body.passwordStu
    console.log(username);
    console.log(password);
    /* if (username == "61160144" && password == "123") {
        res.redirect('/class')
    } */
    if (username && password) {
        con.query('SELECT * FROM student WHERE stu_number = ? AND stu_password = ?', [username, password], (err, result) => {
            if (result.length > 0) {
                stuID = JSON.stringify(result[0].stu_id).replace(/"(.*)"$/, '$1')
                username = JSON.stringify(result[0].stu_number).replace(/"(.*)"$/, '$1')
                password = JSON.stringify(result[0].stu_password).replace(/"(.*)"$/, '$1')
                stuNum = JSON.stringify(result[0].stu_number).replace(/"(.*)"$/, '$1')
                res.redirect('/classStu');
                //console.log(stuID)
            } else {
                res.send('<script>alert("คุณไม่เป็นสมาชิก กรุณาลองอีกครั้ง!"); window.location.href = "/"; </script>');
            }
        });
    } else {
        res.send('<script>alert("กรอกข้อมูลไม่ถูกต้อง กรุณาลองอีกครั้ง!"); window.location.href = "/"; </script>');
    }
})

//แสดงคลาสทั้งหมดของนักเรียน
app.get("/classStu", function (req, res) {
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    //console.log(stuNum)
    con.query('SELECT * FROM student JOIN class ON student.class_id=class.class_id WHERE student.stu_number = ?', [stuNum], (err, resclass) => {
        if (err) console.log(err);
        //console.log(resclass)
        return res.render(__dirname + "/html/" + "mainStudent.html", {
            name: sut_name,
            lastname: lastname,
            classSut: resclass
        })
    })
    //res.sendFile(__dirname + "/html/" + "mainStudent.html");
})

//โชว์เนื้อหาบทเรียนในคลาส ฝั่งนักเรียน
app.get("/ClassLessonSut:id", function (req, res) {
    var { id } = req.params
    id_class = id;
    console.log(id_class)
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT * FROM seclesson WHERE class_id = ? ', [id_class], (err, rows) => {
        if (err) console.log(err);
        con.query('SELECT * FROM lesson', (err, lesson) => {
            if (err) console.log(err);
            return res.render(__dirname + "/html/" + "lessonSut.html", {
                name: sut_name,
                lastname: lastname,
                secLes: rows,
                Les: lesson
            });
        })
    })
})

//โชว์เนื้อหาบทเรียน ฝั่งนักเรียน
app.get("/seclessonstu", function (req, res) {
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT * FROM seclesson WHERE class_id = ? ', [id_class], (err, rows) => {
        if (err) console.log(err);
        con.query('SELECT * FROM lesson', (err, lesson) => {
            if (err) console.log(err);
            return res.render(__dirname + "/html/" + "lessonSut.html", {
                name: sut_name,
                lastname: lastname,
                secLes: rows,
                Les: lesson
            });
        })
    })
    //res.sendFile(__dirname + "/html/" + "lessonSut.html");
})

//แสดงบทเรียนทั้งหมดในหัวข้อ ฝั่งนักเรียน
app.get("/AllLessonSut:id", function (req, res) {
    var { id } = req.params
    id_sectest = id;
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT * FROM lesson INNER JOIN seclesson ON lesson.sec_lesson_id = seclesson.sec_lesson_id WHERE seclesson.sec_lesson_id = ?', [id], (err, result) => {
        if (err) throw err
        var listening = [];
        var speaking = [];
        var reading = [];
        var writing = [];
        for (var i = 0; i < result.length; i++) {
            if (result[i].lesson_type == "ฟัง") {
                listening.push(result[i])
            } else if (result[i].lesson_type == "พูด") {
                speaking.push(result[i])
            } else if (result[i].lesson_type == "อ่าน") {
                reading.push(result[i])
            } else if (result[i].lesson_type == "เขียน") {
                writing.push(result[i])
            }
        }
        return res.render(__dirname + "/html/" + "all-lesson-stu.html", {
            name: sut_name,
            lastname: lastname,
            all: result,
            listen: listening,
            speak: speaking,
            read: reading,
            writ: writing
        })
    })
    //res.sendFile(__dirname + "/html/" + "all-lesson-stu.html");
})
//กดเข้าเรียน แสดงบทเรียน
app.get("/leanSut:id", function (req, res) {
    var { id } = req.params
    console.log(id);
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT * FROM lesson WHERE lesson_id = ? ', [id], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            lesson = row.lesson_name;
            explain = row.lesson_explain;
        });
    })
    con.query('SELECT * FROM vdo WHERE lesson_id = ? ', [id], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            vdo = row.vdo_name;
        });
    })
    //บันทึกการเข้าเรียน
    var stac = {
        stac_num: 1,
        lesson_id: id,
        stu_id: stuID
    }
    var count = '';
    var id_stac
    var idstacs = '';
    var count1 = '';
    var countsum = 0;
    con.query('SELECT * FROM statistics WHERE lesson_id = ? AND  stu_id = ?', [id, stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            id_stac = row.stac_id;
            count = row.stac_num;
        });
        //console.log('count ' + count)
        if (count == '') {
            con.query('INSERT INTO statistics SET ? ', stac, (err) => {
                if (err) throw err;
            });
            con.query('SELECT lesson_id ,COUNT(stu_id) AS total_num FROM statistics WHERE statistics.lesson_id = ?', [id], (err, cou) => {
                console.log(cou)
                con.query('SELECT * FROM statisticssum WHERE lesson_id = ?', [cou[0].lesson_id], (err, rows) => {
                    if (err) console.log(err);
                    rows.forEach((row) => {
                        idstacs = row.lesson_id;
                        countsum += row.total_num;
                        count1 = 1;
                    });
                    console.log('count ' + countsum)
                    console.log('id ' + idstacs)
                    if (count1 == '') {
                        stacsum = {
                            total_num: cou[0].total_num,
                            lesson_id: cou[0].lesson_id
                        }
                        //เพิ่ม ผลรวมเข้าตาราง สถิติผลรวม
                        con.query('INSERT INTO statisticssum SET ? ', stacsum, (err) => {
                            if (err) throw err;
                            console.log(stacsum)
                        });
                    } else {
                        countsum++
                        console.log('count ' + countsum)
                        con.query("UPDATE statisticssum  SET total_num = '" + countsum + "' WHERE lesson_id = '" + idstacs + "'", (err) => {
                            if (err) throw err;
                        });
                    }

                })
            })
        } else {
            con.query("UPDATE statistics SET stac_num = '" + 1 + "',  lesson_id = '" + id + "', stu_id = '" + stuID + "' WHERE stac_id = '" + id_stac + "'", (err) => {
                if (err) throw err;
            });
        }

    })


    con.query('SELECT * FROM doc WHERE lesson_id = ? ', [id], (err, rows) => {
        if (err) console.log(err);
        return res.render(__dirname + "/html/" + "lean-lesson-page.html", {
            name: sut_name,
            lastname: lastname,
            lesson_name: lesson,
            explain: explain,
            vdo: vdo,
            doc: rows
        });
    })
})

//แสดงหัวข้อแบบทดสอบฝั่งนักเรียน
app.get("/secTestSut", function (req, res) {
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT * FROM sectest WHERE class_id = ? ', [id_class], (err, rows) => {
        if (err) console.log(err);
        con.query('SELECT * FROM test', (err, test) => {
            if (err) console.log(err);
            return res.render(__dirname + "/html/" + "testSut-page.html", {
                name: sut_name,
                lastname: lastname,
                secTest: rows,
                nameTest: test
            });
        })
    })
})

//แสดงแบบทดสอบทั้งหมดในหัวข้อ ฝั่งนักเรียน
app.get("/AllTestSut:id", function (req, res) {
    var { id } = req.params
    id_sectest = id;
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT * FROM test INNER JOIN sectest ON test.sec_test_id = sectest.sec_test_id WHERE sectest.sec_test_id = ?', [id], (err, result) => {
        if (err) console.log(err);
        var listening = [];
        var speaking = [];
        var reading = [];
        var writing = [];
        for (var i = 0; i < result.length; i++) {
            if (result[i].test_type == "ฟัง") {
                listening.push(result[i])
            } else if (result[i].test_type == "พูด") {
                speaking.push(result[i])
            } else if (result[i].test_type == "อ่าน") {
                reading.push(result[i])
            } else if (result[i].test_type == "เขียน") {
                writing.push(result[i])
            }
        }
        return res.render(__dirname + "/html/" + "all-test-stu.html", {
            name: sut_name,
            lastname: lastname,
            all: result,
            listen: listening,
            speak: speaking,
            read: reading,
            writ: writing,
        })
    })
    //res.sendFile(__dirname + "/html/" + "all-test-stu.html");
})

app.get("/testEng:id", function (req, res) {
    var { id } = req.params
    id_testSTU = id;
    console.log(id_testSTU)
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT * FROM test WHERE test_id = ?', [id_testSTU], (err, restest) => {
        if (err) throw err
        console.log(restest)
        return res.render(__dirname + "/html/" + "testEng.html", {
            name: sut_name,
            lastname: lastname,
            dt_name: restest[0].test_name,
            dt_expian: restest[0].test_explain,
            dt_doc: restest[0].test_doc,
            dt_form: restest[0].test_form
        })
    })
    //res.sendFile(__dirname + "/html/" + "testEng.html");
})
app.get("/testSpeech:id", function (req, res) {
    var { id } = req.params
    id_testSTU = id;
    console.log(id_testSTU)
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT * FROM `score` JOIN student ON score.stu_id = student.stu_id WHERE score.test_id = ? AND student.stu_number = ?', [id_testSTU, stuNum], (err, row) => {
        console.log(row)
        if (row.length > 0) {
            res.redirect('/AllTestSut' + id_sectest);
        } else {
            con.query('SELECT * FROM test WHERE test_id = ?', [id_testSTU], (err, restest) => {
                if (err) throw err
                return res.render(__dirname + "/html/" + "testSpeech.html", {
                    name: sut_name,
                    lastname: lastname,
                    dt_name: restest[0].test_name,
                    dt_expian: restest[0].test_explain,
                    dt_ver: restest[0].test_ver
                })
            })
        }
    })
    //res.sendFile(__dirname + "/html/" + "testSpeech.html");
})

//แสดงคะแนนฝั่งนักเรียน 
app.get("/scoreSut", function (req, res) {
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('SELECT test.test_id,test.test_name, ( SELECT MAX(score) FROM score WHERE score.test_id = test.test_id ORDER BY score DESC ) as max FROM test JOIN sectest  ON test.sec_test_id = sectest.sec_test_id WHERE sectest.class_id = ? GROUP BY test.test_id ORDER BY max DESC', [id_class], (err, sumScore) => {
        return res.render(__dirname + "/html/" + "scoreStu-page.html", {
            name: sut_name,
            lastname: lastname,
            sumScore: sumScore,
        })
    })

    //res.sendFile(__dirname + "/html/" + "scoreStu-page.html");
})

//หน้าโชว์รายละเอียดคะแนนแต่ละเทส ฝั่งนักเรียน
app.get('/AllscoreStu:id', function (req, res) {
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    var { id } = req.params
    console.log(id)
    con.query('SELECT * FROM `test` WHERE test.test_id = ?', [id], (err, test) => {
        if (err) console.log(err);
        test.forEach((row) => {
            testName = row.test_name;
        });
    })
    con.query('SELECT * FROM score JOIN student ON score.stu_id = student.stu_id JOIN test ON score.test_id = test.test_id WHERE score.test_id = ?', [id], (err, Scorelist) => {
        if (err) throw err
        console.log(Scorelist)
        Scorelist.forEach((row) => {
            nameTest = row.test_name;
        });
        return res.render(__dirname + "/html/" + "allScoreStu", {
            name: sut_name,
            lastname: lastname,
            Scorelist: Scorelist,
            testName: testName
        })
    })
})

app.get("/ranking", function (req, res) {
    con.query('SELECT * FROM student WHERE student.stu_id = ?', [stuID], (err, rows) => {
        if (err) console.log(err);
        rows.forEach((row) => {
            stuID = row.stu_id;
            sut_name = row.stu_name;
            lastname = row.stu_lastname;
            stuNum = row.stu_number;
        });
    })
    con.query('select *, RANK() over ( ORDER BY sumscore DESC )rank, DENSE_RANK() over ( ORDER BY sumscore DESC )dense_rank, row_number() over ( ORDER BY sumscore DESC )row_number from sumscorestu WHERE sumscorestu.class_id = ? ', [id_class], (err, s) => {
        if (err) console.log(err);
        console.log(s)
        return res.render(__dirname + "/html/" + "ranking.html", {
            name: sut_name,
            lastname: lastname,
            rank: s
        })
    })
    //res.sendFile(__dirname + "/html/" + "ranking.html");
})
app.get("/logoutSut", function (req, res) {
    res.redirect("/");
})

app.listen(3000, '127.0.0.1', function () {
    console.log('Server Listen at http://127.0.0.1:3000');
});