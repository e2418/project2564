-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2022 at 08:23 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `engdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `class_id` int(11) NOT NULL,
  `class_name` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `class_year` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `tea_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`class_id`, `class_name`, `class_year`, `tea_id`) VALUES
(1, '1/1', '1', 1),
(2, '1/2', '1', 1),
(3, '2/1', '2', 1),
(4, '2/2', '2', 1),
(5, '3/1', '3', 2),
(6, '3/2', '3', 2),
(7, 'CS', '4', 2);

-- --------------------------------------------------------

--
-- Table structure for table `doc`
--

CREATE TABLE `doc` (
  `doc_id` int(11) NOT NULL,
  `doc_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc`
--

INSERT INTO `doc` (`doc_id`, `doc_name`, `lesson_id`) VALUES
(1, 'ใบงานประกอบการสอน_เรื่อง_My_Family-03071526.pdf', 1),
(2, 'สื่อประกอบการสอน_เรื่อง_My_Family-03071523.pdf', 1),
(3, 'ใบงานประกอบการสอน_เรื่อง_Variety_of_Food-03141908.pdf', 2),
(4, 'สื่อประกอบการสอน_เรื่อง_Variety_of_Food-03141907.pdf', 2),
(5, 'ใบงานประกอบการสอน_เรื่อง_Variety_of_Drinks-03141918.pdf', 3),
(6, 'สื่อประกอบการสอน_เรื่อง_Variety_of_Drinks-03141918.pdf', 3),
(7, 'ใบงานประกอบการสอน_เรื่อง_Tastes_of_Food-03141923.pdf', 4),
(8, 'สื่อประกอบการสอน_เรื่อง_Tastes_of_Food-03141923.pdf', 4),
(9, 'สื่อประกอบการสอน_เรื่อง_Weather___Seasons_1-03141932.pdf', 5),
(10, 'ใบงานประกอบการสอน_เรื่อง_เรื่อง_The_Sun_Comes_up!-03071552.pdf', 6),
(11, 'สื่อประกอบการสอน_เรื่อง_The_Sun_Comes_up!-03071552.pdf', 6),
(12, 'สื่อประกอบการสอน_เรื่อง_Weather___Seasons_1-03141932.pdf', 7);

-- --------------------------------------------------------

--
-- Table structure for table `lesson`
--

CREATE TABLE `lesson` (
  `lesson_id` int(11) NOT NULL,
  `lesson_name` char(30) CHARACTER SET utf8 DEFAULT NULL,
  `lesson_explain` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `lesson_type` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `sec_lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lesson`
--

INSERT INTO `lesson` (`lesson_id`, `lesson_name`, `lesson_explain`, `lesson_type`, `sec_lesson_id`) VALUES
(1, 'My Family', 'การเรียนรู้คำศัพท์และประโยคเกี่ยวกับสมาชิกในครอบครัว พ่อ แม่ ปู่ ย่า ตา ยาย  และการพูดแนะนำสมาชิกในครอบครัว  การพูดให้ข้อมูลเกี่ยวกับจำนวนสมาชิกในครอบครัว  ทำให้นักเรียนได้ฝึกใช้ภาษาในสถานการณ์จริงในห', 'พูด', 1),
(2, 'Variety of Food', 'ในการเรียนภาษาอังกฤษ การเรียนรู้คำศัพท์และโครงสร้างไวยากรณ์ต่างๆจะช่วยให้นักเรียนสามารถอ่าน และเข้าใจเนื้อเรื่องที่อ่านได้ดีและ เป็นพื้นฐานในการใช้ภาษาอังกฤษเพื่อการสื่อสารในระดับที่สูงขึ้นอีกทั้งยังน', 'ฟัง', 1),
(3, 'Variety of  Drinks', 'การสอบถามเกี่ยวกับอาหารและเครื่องดื่มทำให้นักเรียนได้ฝึกใช้ภาษาในสถานการณ์จริงในห้องเรียนและสามารถนำไปใช้ในการสื่อสารกับบุคคลภายนอก ได้อย่างมั่นใจ', 'อ่าน', 1),
(4, 'Tastes of  Food', 'การออกเสียงคำศัพท์และการเรียนรู้ประโยค โครงสร้างในการสนทนาถามตอบเกี่ยวกับรสชาติ ของอาหารและฝึกภาษาในสถานการณ์จริงในห้องเรียนทำให้นักเรียนมีความรู้และพัฒนาทักษะภาษาสามารถนำไปใช้ในการสื่อสารกับบุคคลภายน', 'ฟัง', 1),
(5, 'Weather & Seasons', 'การใช้คำศัพท์และโครงสร้างประโยคในการถาม-ตอบเกี่ยวกับสภาพอากาศ ฤดูกาล และการแต่งกายให้เหมาะสมกับสภาพอากาศ', 'เขียน', 1),
(6, 'The Sun Comes up!', 'การใช้คำศัพท์และโครงสร้างประโยคในการให้ข้อมูลเกี่ยวกับสภาพอากาศ ฤดูกาล การแต่งกายและกิจกรรมที่จะทำในแต่ละฤดูกาลที่เหมาะสม เป็นการฝึกทักษะการใช้ภาษาอังกฤษในการสื่อสารเพื่อให้ข้อมูลเกี่ยวกับเรื่องต่าง ๆ', 'เขียน', 1),
(7, 'Weather & Seasons', 'การใช้คำศัพท์และโครงสร้างประโยคในการให้ข้อมูลเกี่ยวกับสภาพอากาศ ฤดูกาล การแต่งกายและกิจกรรมที่จะทำในแต่ละฤดูกาลที่เหมาะสม เป็นการฝึกทักษะการใช้ภาษาอังกฤษในการสื่อสารเพื่อให้ข้อมูลเกี่ยวกับเรื่องต่าง ๆ', 'ฟัง', 2);

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE `score` (
  `score_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `stu_id` int(11) NOT NULL,
  `score` int(20) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `score`
--

INSERT INTO `score` (`score_id`, `test_id`, `stu_id`, `score`, `class_id`) VALUES
(1, 1, 1, 16, 5),
(2, 1, 2, 20, 5),
(3, 1, 3, 15, 5),
(4, 1, 4, 18, 5),
(5, 1, 5, 18, 5),
(6, 1, 6, 12, 5),
(7, 1, 7, 15, 5),
(8, 1, 8, 20, 5),
(9, 1, 9, 20, 5),
(10, 1, 10, 19, 5),
(11, 3, 1, 10, 5),
(12, 3, 2, 7, 5),
(13, 3, 3, 8, 5),
(14, 3, 4, 6, 5),
(15, 3, 5, 10, 5),
(16, 3, 6, 6, 5),
(17, 2, 1, 20, 5),
(18, 2, 2, 20, 5),
(19, 2, 3, 18, 5),
(20, 2, 4, 20, 5),
(21, 2, 5, 20, 5),
(22, 2, 6, 14, 5),
(23, 2, 7, 20, 5),
(24, 3, 7, 10, 5),
(25, 4, 1, 3, 5),
(26, 4, 2, 5, 5),
(27, 4, 3, 5, 5),
(28, 4, 4, 5, 5),
(29, 2, 8, 10, 5),
(30, 2, 9, 14, 5),
(31, 2, 10, 17, 5),
(32, 3, 8, 8, 5),
(33, 3, 9, 6, 5),
(34, 3, 10, 10, 5),
(35, 4, 5, 8, 5),
(36, 4, 6, 2, 5),
(37, 4, 7, 5, 5),
(38, 4, 8, 10, 5),
(39, 4, 10, 10, 5),
(40, 6, 11, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `scorestac`
--

CREATE TABLE `scorestac` (
  `scorestac_id` int(11) NOT NULL,
  `score_min` int(20) NOT NULL,
  `score_max` int(20) NOT NULL,
  `score_mean` int(20) NOT NULL,
  `test_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `scorestac`
--

INSERT INTO `scorestac` (`scorestac_id`, `score_min`, `score_max`, `score_mean`, `test_id`) VALUES
(1, 12, 20, 17, 1),
(2, 10, 20, 17, 2),
(3, 6, 10, 8, 3),
(4, 2, 10, 6, 4),
(5, 0, 0, 0, 5),
(6, 1, 1, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `seclesson`
--

CREATE TABLE `seclesson` (
  `sec_lesson_id` int(11) NOT NULL,
  `sec_name` char(50) CHARACTER SET utf8 NOT NULL,
  `post_date` date NOT NULL DEFAULT current_timestamp(),
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `seclesson`
--

INSERT INTO `seclesson` (`sec_lesson_id`, `sec_name`, `post_date`, `class_id`) VALUES
(1, 'หน่วยการเรียนรู้ที่ 1', '2022-04-05', 5),
(2, 'หน่วยการเรียนรู้ที่ 1', '2022-04-09', 7);

-- --------------------------------------------------------

--
-- Table structure for table `sectest`
--

CREATE TABLE `sectest` (
  `sec_test_id` int(11) NOT NULL,
  `sec_test_name` char(50) CHARACTER SET utf8 NOT NULL,
  `post_date` date NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sectest`
--

INSERT INTO `sectest` (`sec_test_id`, `sec_test_name`, `post_date`, `class_id`) VALUES
(1, 'แบบทดสอบที่ 1', '2022-04-05', 5),
(2, 'แบบทดสอบที่ 1', '2022-04-09', 7);

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `stac_id` int(11) NOT NULL,
  `stac_num` int(100) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `stu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `statistics`
--

INSERT INTO `statistics` (`stac_id`, `stac_num`, `lesson_id`, `stu_id`) VALUES
(1, 1, 2, 1),
(2, 1, 2, 2),
(3, 1, 2, 3),
(4, 1, 2, 4),
(5, 1, 2, 5),
(6, 1, 2, 6),
(7, 1, 2, 7),
(8, 1, 2, 8),
(9, 1, 2, 9),
(10, 1, 2, 10),
(11, 1, 3, 1),
(12, 1, 5, 1),
(13, 1, 6, 1),
(14, 1, 1, 1),
(15, 1, 4, 1),
(16, 1, 3, 2),
(17, 1, 1, 2),
(18, 1, 6, 2),
(19, 1, 4, 2),
(20, 1, 5, 2),
(21, 1, 3, 3),
(22, 1, 1, 3),
(23, 1, 6, 3),
(24, 1, 5, 3),
(25, 1, 4, 3),
(26, 1, 3, 4),
(27, 1, 1, 4),
(28, 1, 5, 4),
(29, 1, 4, 4),
(30, 1, 6, 4),
(31, 1, 6, 5),
(32, 1, 3, 5),
(33, 1, 5, 5),
(34, 1, 4, 5),
(35, 1, 1, 5),
(36, 1, 6, 6),
(37, 1, 3, 6),
(38, 1, 5, 6),
(39, 1, 4, 6),
(40, 1, 1, 6),
(41, 1, 6, 7),
(42, 1, 5, 7),
(43, 1, 3, 7),
(44, 1, 1, 7),
(45, 1, 4, 7),
(46, 1, 4, 8),
(47, 1, 1, 8),
(48, 1, 3, 8),
(49, 1, 5, 8),
(50, 1, 6, 8),
(51, 1, 4, 9),
(52, 1, 1, 9),
(53, 1, 3, 9),
(54, 1, 5, 9),
(55, 1, 6, 9),
(56, 1, 4, 10),
(57, 1, 1, 10),
(58, 1, 3, 10),
(59, 1, 5, 10),
(60, 1, 6, 10);

-- --------------------------------------------------------

--
-- Table structure for table `statisticssum`
--

CREATE TABLE `statisticssum` (
  `statisticsSum_id` int(11) NOT NULL,
  `total_num` int(100) NOT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `statisticssum`
--

INSERT INTO `statisticssum` (`statisticsSum_id`, `total_num`, `lesson_id`) VALUES
(1, 10, 2),
(2, 10, 3),
(3, 10, 5),
(4, 10, 6),
(5, 10, 1),
(6, 10, 4);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `stu_id` int(11) NOT NULL,
  `stu_name` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `stu_lastname` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `stu_number` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `stu_password` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `stu_classname` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`stu_id`, `stu_name`, `stu_lastname`, `stu_number`, `stu_password`, `stu_classname`, `class_id`) VALUES
(1, 'นิวงษา', 'ชัยงาม', '63616001', '63616001', '3/1', 5),
(2, 'พิทยา', 'บุษบา', '63616002', '63616002', '3/1', 5),
(3, 'ลักษิกา', 'สังสี', '63616003', '63616003', '3/1', 5),
(4, 'เปรมสุดา', 'น้อยอ่อน', '63616004', '63616004', '3/1', 5),
(5, 'สุพรรณษา', 'นุชสา', '63616005', '63616005', '3/1', 5),
(6, 'ปฏิพล', 'นบน้อม', '63616006', '63616006', '3/1', 5),
(7, 'วีรวุฒ', 'เดชาโมรกุล', '63616007', '63616007', '3/1', 5),
(8, 'ขวัญกมล', 'พันธุ์มณี', '63616008', '63616008', '3/1', 5),
(9, 'ณัฐปภัสร์', 'ใจอุ่น', '63616009', '63616009', '3/1', 5),
(10, 'นภาวรรณ', 'ขำปริก', '63616010', '63616010', '3/1', 5),
(11, 'A', 'A', '64616001', '64616001', 'CS', 7);

-- --------------------------------------------------------

--
-- Table structure for table `sumscorestu`
--

CREATE TABLE `sumscorestu` (
  `sumscorestu_id` int(11) NOT NULL,
  `stu_id` int(11) NOT NULL,
  `name_stu` char(20) CHARACTER SET utf8 NOT NULL,
  `last_stu` char(20) CHARACTER SET utf8 NOT NULL,
  `sumscore` int(100) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sumscorestu`
--

INSERT INTO `sumscorestu` (`sumscorestu_id`, `stu_id`, `name_stu`, `last_stu`, `sumscore`, `class_id`) VALUES
(1, 1, 'นิวงษา', 'ชัยงาม', 49, 5),
(2, 2, 'พิทยา', 'บุษบา', 52, 5),
(3, 3, 'ลักษิกา', 'สังสี', 46, 5),
(4, 4, 'เปรมสุดา', 'น้อยอ่อน', 49, 5),
(5, 5, 'สุพรรณษา', 'นุชสา', 56, 5),
(6, 6, 'ปฏิพล', 'นบน้อม', 34, 5),
(7, 7, 'วีรวุฒ', 'เดชาโมรกุล', 50, 5),
(8, 8, 'ขวัญกมล', 'พันธุ์มณี', 48, 5),
(9, 9, 'ณัฐปภัสร์', 'ใจอุ่น', 40, 5),
(10, 10, 'นภาวรรณ', 'ขำปริก', 56, 5),
(11, 11, 'A', 'A', 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `tea_id` int(11) NOT NULL,
  `tea_firstname` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `tea_lastname` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `tea_username` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `tea_password` char(20) CHARACTER SET utf8 DEFAULT NULL,
  `tea_image` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`tea_id`, `tea_firstname`, `tea_lastname`, `tea_username`, `tea_password`, `tea_image`) VALUES
(1, 'เมกุโระ', 'เร็น', '256501', '1234', 'avatar-icon.png'),
(2, 'น้ำหวาน', 'สันติวัฒนา', '256502', '1234', 'avatar-icon.png'),
(3, 'แจ๊คกี้', 'ชาเคอลีน', '256503', '1234', 'avatar-icon.png');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `test_id` int(11) NOT NULL,
  `sec_test_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `stu_id` int(11) NOT NULL,
  `test_name` char(30) CHARACTER SET utf8 NOT NULL,
  `test_explain` varchar(200) CHARACTER SET utf8 NOT NULL,
  `test_type` char(20) CHARACTER SET utf8 NOT NULL,
  `test_doc` text NOT NULL,
  `test_form` text NOT NULL,
  `test_ver` text NOT NULL,
  `test_score` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`test_id`, `sec_test_id`, `lesson_id`, `stu_id`, `test_name`, `test_explain`, `test_type`, `test_doc`, `test_form`, `test_ver`, `test_score`) VALUES
(1, 1, 2, 0, 'Variety of Food', 'ให้นักเรียนเข้าทำแบบทดสอบใน google forms เพื่อเก็บคะแนน \r\nนักเรียนสามารถกดส่งคำตอบได้ครั้งเดียวเท่านั้น หากนักเรียนส่งซ้ำจะเก็บคะแนนที่ส่งครั้งแรกเป็นหลัก', 'ฟัง', '', 'https://forms.gle/CTHiAR5qDZ8ai8aw6', '', 20),
(2, 1, 3, 0, 'Variety of  Drinks', 'ให้นักเรียนเข้าทำแบบทดสอบใน google forms เพื่อเก็บคะแนน \r\nนักเรียนสามารถกดส่งคำตอบได้ครั้งเดียวเท่านั้น หากนักเรียนส่งซ้ำจะเก็บคะแนนที่ส่งครั้งแรกเป็นหลัก', 'อ่าน', '', 'https://forms.gle/ZyNCEAbYT9GWZzGf6', '', 20),
(3, 1, 6, 0, 'The Sun Comes up!', 'ให้นักเรียนเข้าทำแบบทดสอบใน google forms เพื่อเก็บคะแนน \r\nนักเรียนสามารถกดส่งคำตอบได้ครั้งเดียวเท่านั้น หากนักเรียนส่งซ้ำจะเก็บคะแนนที่ส่งครั้งแรกเป็นหลัก', 'เขียน', '', 'https://forms.gle/YLn7D3ENu8EKrAw98', '', 10),
(4, 1, 1, 0, 'My Family', 'ความถูกต้องในการออกเสียงคำศัพท์เกี่ยวกับสมาชิกในครอบครัว  ', 'พูด', '', '', 'grandfather,grandmother,father,mother,brother,sister,uncle,me,parents,son', 10),
(5, 2, 7, 0, 'Variety of Fruit', 'ให้นักเรียนเข้าทำแบบทดสอบใน google forms เพื่อเก็บคะแนน \r\nนักเรียนสามารถกดส่งคำตอบได้ครั้งเดียวเท่านั้น หากนักเรียนส่งซ้ำจะเก็บคะแนนที่ส่งครั้งแรกเป็นหลัก', 'อ่าน', '', 'https://forms.gle/CTHiAR5qDZ8ai8aw6', '', 10),
(6, 2, 7, 0, 'Variety of Food', 'fdg', 'พูด', '', '', 'apple,banana', 2);

-- --------------------------------------------------------

--
-- Table structure for table `vdo`
--

CREATE TABLE `vdo` (
  `vdo_id` int(11) NOT NULL,
  `vdo_name` varchar(255) NOT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vdo`
--

INSERT INTO `vdo` (`vdo_id`, `vdo_name`, `lesson_id`) VALUES
(1, 'My_Family___37014.mp4', 1),
(2, 'Variety_of_Food__41409.mp4', 2),
(3, 'Variety_of_Drinks__41431.mp4', 3),
(4, 'Tastes_of_Food__41862.mp4', 4),
(5, 'Weather_&_Seasons_1_42980.mp4', 5),
(6, 'The_Sun_Comes_up!_43065.mp4', 6),
(7, 'Variety_of_Food__41409.mp4', 7),
(8, 'My_Family___37014.mp4', 8),
(9, 'Weather_&_Seasons_1_42980.mp4', 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`class_id`),
  ADD KEY `tea_id` (`tea_id`);

--
-- Indexes for table `doc`
--
ALTER TABLE `doc`
  ADD PRIMARY KEY (`doc_id`),
  ADD KEY `lesson_id` (`lesson_id`);

--
-- Indexes for table `lesson`
--
ALTER TABLE `lesson`
  ADD PRIMARY KEY (`lesson_id`),
  ADD KEY `sec_lesson_id` (`sec_lesson_id`);

--
-- Indexes for table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`score_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `stu_id` (`stu_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `scorestac`
--
ALTER TABLE `scorestac`
  ADD PRIMARY KEY (`scorestac_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `seclesson`
--
ALTER TABLE `seclesson`
  ADD PRIMARY KEY (`sec_lesson_id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `sectest`
--
ALTER TABLE `sectest`
  ADD PRIMARY KEY (`sec_test_id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`stac_id`),
  ADD KEY `lesson_id` (`lesson_id`),
  ADD KEY `stu_id` (`stu_id`);

--
-- Indexes for table `statisticssum`
--
ALTER TABLE `statisticssum`
  ADD PRIMARY KEY (`statisticsSum_id`),
  ADD KEY `lesson_id` (`lesson_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`stu_id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `sumscorestu`
--
ALTER TABLE `sumscorestu`
  ADD PRIMARY KEY (`sumscorestu_id`),
  ADD KEY `stu_id` (`stu_id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`tea_id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`test_id`),
  ADD KEY `sec_test_id` (`sec_test_id`),
  ADD KEY `lesson_id` (`lesson_id`),
  ADD KEY `sut_id` (`stu_id`);

--
-- Indexes for table `vdo`
--
ALTER TABLE `vdo`
  ADD PRIMARY KEY (`vdo_id`),
  ADD KEY `lesson_id` (`lesson_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `doc`
--
ALTER TABLE `doc`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lesson`
--
ALTER TABLE `lesson`
  MODIFY `lesson_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `score`
--
ALTER TABLE `score`
  MODIFY `score_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `scorestac`
--
ALTER TABLE `scorestac`
  MODIFY `scorestac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `seclesson`
--
ALTER TABLE `seclesson`
  MODIFY `sec_lesson_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sectest`
--
ALTER TABLE `sectest`
  MODIFY `sec_test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `stac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `statisticssum`
--
ALTER TABLE `statisticssum`
  MODIFY `statisticsSum_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `stu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sumscorestu`
--
ALTER TABLE `sumscorestu`
  MODIFY `sumscorestu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `tea_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vdo`
--
ALTER TABLE `vdo`
  MODIFY `vdo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
